!% -Cu
!% -v5
!% -~S
!% -~D
!Source encoding UTF-8 No Mark

!///////////////////////////////////////////////////////////////////////////////
!MAGIC MIRROR, an Interactive Fiction by Michael Taylor (1982)
!http://www.miketaylor.org.uk/tech/advent/mm/
!From Dave Smythe Python port 
!///////////////////////////////////////////////////////////////////////////////
! ------------------------------------------------------------------------------
#Ifdef TARGET_ZCODE;
! ------------------------------------------------------------------------------
Constant DICT_VERB $01;
Constant DICT_NOUN $80;
Constant INPUT_BUFFER_LEN = WORDSIZE + 120;
Constant MAX_BUFFER_WORDS = 3;

Constant HDR_GAMERELEASE  $02;
Constant HDR_GAMESERIAL   $12;
Constant HDR_DICTIONARY   $08;
Constant HDR_STATICMEMORY $0E;
Constant HDR_GAMEFLAGS    $10;
Constant RADICALMIN 3;

Constant Story "MAGIC MIRROR";
Constant Headline "An Interactive Fiction by Michael Taylor (1982)";

!whether the ogre is 'quelled' (i.e., you have given him the bottle of whiskey)
Constant ogreQuelled        $$0000000000000001;
! whether manhole is open
Constant manholeOpen        $$0000000000000010;
! whether manhole is unlocked
Constant manholeUnlocked    $$0000000000000100;
Constant windowOpen         $$0000000000001000;
Constant lookedInCellar     $$0000000000010000;
Constant torchIsLit         $$0000000000100000;
! whether the dwarf in the way is dead
Constant dwarfIsDead        $$0000000001000000;
! whether the guard is 'adorned' (i.e., you have given him the golden medal)
Constant guardIsAdorned     $$0000000010000000;
Constant promptEnteringRoom $$0000000100000000;
! ------------------------------------------------------------------------------
Global vb;
Global nn;
Global newRoom;
Global isDark;
Global deadflag;
Global riverCrossingDirection;
Global itemsCarried;
Global currentLocation;
Global flags;

Global dict_start;
Global dict_entry_size;
Global dict_end;
Global transcript_mode;
Global xcommsdir;
! ------------------------------------------------------------------------------
Array buffer -> INPUT_BUFFER_LEN + 1;
Array parse buffer (MAX_BUFFER_WORDS * 4) + 3;
Array StorageForShortName buffer 160;
! ------------------------------------------------------------------------------
Include "objets.inf";
! ------------------------------------------------------------------------------
[ DicMot w; return parse-->(w * 2 - 1); ];
[ NbrMot; return parse->1; ];
[ LenMot mot_n; return parse->(4*mot_n); ];
[ PosMot mot_n; return parse->(4*mot_n+1);];
! ------------------------------------------------------------------------------
[ SetFlag flag; flags = flags | flag; ];
[ ClrFlag flag; flags = flags & (~flag); ];
[ TestFlag flag; return (flags & flag); ];
! ------------------------------------------------------------------------------
[ IdObj name_o   o n i;
   objectloop (o provides name) {
      n = o.#name / WORDSIZE;
      for(i = 0 : i < n : i++)
         if (o.&name-->i == name_o) return o;
   }
   rfalse;
];
! ------------------------------------------------------------------------------
[ No__Dword n; return HDR_DICTIONARY-->0 + 7 + 9*n; ];
! ------------------------------------------------------------------------------
[ UnknownWord wn flag   w at len len_dic i j;
   at = PosMot(wn);
   len = LenMot(wn);
   len_dic = (dict_end-dict_start)/dict_entry_size;
   for (i = 0 : i < len_dic : i++) {
      w = No__Dword(i);
      if ( (w->#dict_par1) & flag ) {
         @output_stream 3 StorageForShortName;
         print (address) w;
         @output_stream -3;
         if (len > StorageForShortName-->0) continue;
         j = 0;
         while (j < len) {
            if (buffer->(j+at) ~= StorageForShortName->(j+WORDSIZE)) break;
            j++;
         }
         if (j < len) continue;
         if (j < RADICALMIN && len ~= StorageForShortName-->0) continue;
         return w;
      }
   }
   rfalse;
];
! ------------------------------------------------------------------------------
[ PfVerbsSub;
   font off;
   print
     "Look examine read^
      go   climb   jump swim follow^
      get  wear    drop give throw^
      open unlock  move chop^
      say  kill^
      wait^inventory^
      quit save    load help verbs^";
   font on;
   rfalse;
];

[ pbuf wn   at lg;
   at = buffer + PosMot(wn);
   lg = at + LenMot(wn);
   for ( : at < lg : at++)
      print (char) at->0;
];
! ------------------------------------------------------------------------------
[ Icant   id;
   id = IdObj(nn);
   print "I can't ", (address) vb, " ", (string) id.describe, "^";
];
! ------------------------------------------------------------------------------
[ Synonym w;
   switch(w) {
      'n//': w = 'north';
      's//': w = 'south';
      'e//': w = 'east';
      'w//': w = 'west';
   }
   return w;
];
! ------------------------------------------------------------------------------
[ GetInput   n;
   .FreshInput;
   print "^>";
   read buffer parse;
   
   n = NbrMot();
   if (n == 0) {
      print "Nothing to do!^";
      jump FreshInput;
   }
   if (n > 2) {
      print "Use no more than 2 words.^";
      jump FreshInput;
   }     
 
   vb = DicMot(1);
   vb = Synonym(vb);
   nn = ',*';
   
   if (n == 1 && vb == 'north' or 'south' or 'east' or 'west') {
      nn = vb;
      vb = 'go';
      return;
   }
   
   if (vb == 0) vb = UnknownWord(1, DICT_VERB);
   
   if (vb == 0 || ((vb->#dict_par1) & DICT_VERB) == 0) {
      print "I don't know how to '", (pbuf) 1,"'";
      if (n == 2)
         print " something";
      print ".^";
      jump FreshInput;
   }
   
   if (n == 1) return;

   nn = DicMot(2);
   nn = Synonym(nn);
   if (nn == 0) nn = UnknownWord(2, DICT_NOUN);
   if ( (nn == 0 || ((nn->#dict_par1) & DICT_VERB)) && vb ~= 'say' ) {
      print "I don't know what '", (pbuf) 2,"' is.^";
      jump FreshInput;
   }
];
! ------------------------------------------------------------------------------
[ VerbAction   i syntax action;
   i = $ff-(vb->#dict_par2);
   syntax = (HDR_STATICMEMORY-->0)-->i;
   action = syntax->8;
   action = #actions_table-->action;
   return indirect(action);
];
! ------------------------------------------------------------------------------
[ Banner   i;
   style bold; print (string) Story, "^"; style roman;
   print (string) Headline, "^";
   print "Release ", (HDR_GAMERELEASE-->0) & $03ff, " / Serial number ";
   for ( i = 0 : i < 6 : i++ ) print (char) HDR_GAMESERIAL->i;
   print " / Inform v"; inversion;
   #Ifdef STRICT_MODE;
   print " S";
   #Endif; ! STRICT_MODE
   new_line;
];
! ------------------------------------------------------------------------------
[ KeyTimerInterrupt; rtrue; ];
[ KeyDelay tenths  key;
    @read_char 1 tenths KeyTimerInterrupt -> key;
    !return key;
];
! ------------------------------------------------------------------------------
[ ListObj   i n;
   n = children(currentLocation);
   if (n) {
      print "I can see ";
      objectloop (i in currentLocation) {
         print (string) i.describe;
         if (n == 2) print " and ";
         if (n > 2) print ", ";
         n--;
      }
      print ".^";
   }  
];
! ------------------------------------------------------------------------------
[ ListIssues   i n;
    for (i = n_to : i<= w_to : i++)
       if (currentLocation.i ~= 0) n++;
    if (n) {
       print "I can go ";
       for (i = n_to : i<= w_to : i++) {
          if (currentLocation.i) {
            if (i == n_to) print "north";
            if (i == s_to) print "south";
            if (i == e_to) print "east";
            if (i == w_to) print "west";
            if (n == 2) print " and ";
            if (n > 2) print ", ";
            n--;
         }
       }
       print ".^";
    }
];
! ------------------------------------------------------------------------------
[ Enter_room   key;
   if (TestFlag(promptEnteringRoom)) {
      ClrFlag(promptEnteringRoom);
      print "  Press 'RETURN' ";
      @read_char 1 ->key;
      new_line;
      new_line;
   }
   ! Check for dark
   isDark = false;
   if (currentLocation has normallyDark && ( (~~TestFlag(torchIsLit) )
         || (torch hasnt carrying && torch notin currentLocation)))
      isDark=True;
   if (isDark)
      print "It's too dark to see!^";
   else {
      style bold;
      print "I'm ", (string) currentLocation.describe,"^";
      style roman;
      ListObj();
      ListIssues();
   }
];
! ------------------------------------------------------------------------------
[ gameLoop;
   newRoom = 1;
   while(~~deadflag) {
      if (newRoom)
         enter_room();
      GetInput();
      newRoom = VerbAction();
   }
];
! ------------------------------------------------------------------------------
[ Initialise;
   riverCrossingDirection = -1;
   itemsCarried = 0;
   currentLocation = gianthall;
   flags = 0;
   IsDark = 0;
   deadflag = 0;
   Banner();
   new_line;
];
! ------------------------------------------------------------------------------
[ main   key;
   buffer->0 = INPUT_BUFFER_LEN - WORDSIZE;
   parse->0 = MAX_BUFFER_WORDS;
   
   dict_start = HDR_DICTIONARY-->0;
   dict_entry_size = dict_start->(dict_start->0 + 1);
   dict_start = dict_start + dict_start->0 + 4;
   dict_end = dict_start + (dict_start - 2)-->0 * dict_entry_size;
   
   .start;
   Initialise();
   gameLoop();
   print "The Adventure is over.  Play again? ";
   .readkey;
   @read_char 1 ->key;
   if (key == 'y') {
      new_line;
      new_line;
      jump start;
   }
   if (key ~= 'n')
      jump readkey;
   new_line;
];
! ------------------------------------------------------------------------------
[ ScriptOnSub;
   transcript_mode = ((HDR_GAMEFLAGS-->0) & 1);
   if (transcript_mode) {
      print "Transcripting is already on.^";
      rfalse;
   }
   @output_stream 2;
   if (((HDR_GAMEFLAGS-->0) & 1) == 0) {
      print "Attempt to begin transcript failed.^";
      rfalse;
   }
   print "Start of a transcript of^";
   transcript_mode = true;
   rfalse;
];
! ------------------------------------------------------------------------------
[ ScriptOffSub;
   transcript_mode = ( (HDR_GAMEFLAGS-->0) & 1 );
   if (transcript_mode == false) {
      print "Transcripting is already off.^";
      rfalse;
   }
   print "End of transcript.^";
   @output_stream -2;
   if ((HDR_GAMEFLAGS-->0) & 1) {
      print "Attempt to end transcript failed.^";
      rfalse;
   }
   transcript_mode = false;
   rfalse;
];
! ------------------------------------------------------------------------------
[ CommandsReadSub;
   @input_stream 1;
   xcommsdir = 2;
   print "Replaying commands.^";
   rfalse;
];
! ------------------------------------------------------------------------------
[ HelpSub;
   print "Gotta figure this one out on yer own... Try 'verbs'.^";
   rfalse;
];
! ------------------------------------------------------------------------------
[ GameoverSub;
   deadflag = 1;
   rfalse;
];
! ------------------------------------------------------------------------------
[ SavegameSub   flag;
   @save -> flag;
   switch (flag) {
      0: print "Save failed.^";
      1: print "Ok.^"; SetFlag(promptEnteringRoom); rtrue;
      2: print "Ok.^"; SetFlag(promptEnteringRoom); rtrue; ! Restore
   }
   SetFlag(promptEnteringRoom); 
   rfalse;
];
! ------------------------------------------------------------------------------
[ ReadsavedgameSub;
   restore Rmaybe;
   print "Restore failed.^";
   SetFlag(promptEnteringRoom);
   rtrue;
   .RMaybe;
   print "Ok.^";
   SetFlag(promptEnteringRoom);
   rtrue;
];
! ------------------------------------------------------------------------------
[ GoSub   newloc;
   if (nn == 'north' or 'south' or 'east' or 'west')
      return movement();
      
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (nn == 'window') {
      if (currentLocation ~= attic || (~~TestFlag(windowOpen)) ) {
         print "Not now ...^";
         rfalse;
      }
      currentLocation = hugegarden;
      rtrue;
   }
   if (nn == 'larder') {
      if (currentLocation ~= kitchen) {
         print "Not now ...^";
         rfalse;
      }
      currentLocation = larder_rm;
      rtrue;
   }
   if (nn == 'rosebed' || nn == 'bed') {
      if (currentLocation ~= garden1) {
         print "Not now ...^";
         rfalse;
      }
      currentLocation = rose_bed;
      rtrue;
   }
   if (nn == 'trapdoor') {
      if (currentLocation ~= bedroom && currentLocation ~= cellar) {
         print "Not now ...^";
         rfalse;
      }
      if (currentLocation == bedroom)
         currentLocation = cellar;
      else
         currentLocation = bedroom;
      rtrue;
   }  
   if (nn == 'lake') {
      if (currentLocation ~= sublake && currentLocation ~= bysublake) {
         print "Not now ...^";
         rfalse;
      }
      if (canoe hasnt carrying) {
         print "I need a Boat.^";
         rfalse;
      }  
      currentLocation = canoe_rm;
      rtrue;
   }
   if (nn == 'manhole') {
      if (currentLocation ~= ladder_rm && currentLocation ~= emptyroad) {
         print "Not now ...^";
         rfalse;
      }
      if (~~TestFlag(manholeOpen)) {
         print "Not now ...^";
         rfalse;
      }
      if (currentLocation == ladder_rm) {
         currentLocation = emptyroad;
         move manhole to emptyroad;
         print "OK. Below me, the ladder disappears!^";
         move ladder to rose_bed;
         SetFlag(promptEnteringRoom);
         rtrue;
      }
      print "It's a long fall ... I break my Neck!^";
      print "I'm dead!^";
      GameOverSub();
      rfalse;
   }
   if (nn == 'shack') {
      if (currentLocation ~= quarry) {
         print "Not now ...^";
         rfalse;
      }
      currentLocation = shack_rm;
      rtrue;
   }
   if (nn == 'pond') {
      if (currentLocation ~= pathend) {
         print "Not now ...^";
         rfalse;
      }
      newLoc = dampchamber;
   }
   else {
      if (nn ~= 'pool') {
         print "I can't go there.^";
         rfalse;
      }
      if (currentLocation ~= courtyard) {
         print "Not now ...^";
         rfalse;
      }
      newLoc = pathend;
   }
   if (aqualung hasnt wearing) {
      print "No air! ... I drown!^";
      print "I'm dead!^";
      GameOverSub();
      rfalse;
   }
   currentLocation = newLoc;
   print "OK.^";
   KeyDelay(15);
   print "I surface.^";
   SetFlag(promptEnteringRoom);
   rtrue;
];
! ------------------------------------------------------------------------------
[ Movement;
   if (nn == ',*') nn = vb;
   if ((~~TestFlag(dwarfIsDead)) && currentLocation == roadbend && nn == 'west') {
      print "The Dwarf bars my way!^";
      rfalse;
   }
   if ((~~TestFlag(ogreQuelled)) && currentLocation == ledge) {
      print "The Ogre bars my way!^";
      rfalse;
   }
   if ((~~TestFlag(guardIsAdorned)) && currentLocation == palace && nn == 'south') {
      print "The Guard bars my way^";
      rfalse;
   }
   if (nn == 'north' && currentLocation provides n_to && currentLocation.n_to) {
      currentLocation = currentLocation.n_to;
      rtrue;
   }
   if (nn == 'south' && currentLocation provides s_to && currentLocation.s_to) {
      currentLocation = currentLocation.s_to;
      rtrue;
   }
   if (nn == 'east' && currentLocation provides e_to && currentLocation.e_to) {
      currentLocation = currentLocation.e_to;
      rtrue;
   }
   if (nn == 'west' && currentLocation provides w_to && currentLocation.w_to) {
      currentLocation = currentLocation.w_to;
      rtrue;
   }
   if (~~isDark) {
      print "I can't go there.^";
      rfalse;
   }
   ! fall in the dark...
   print "I fall and break my   Neck!^";
   GameOverSub();
   rfalse;
];
! ------------------------------------------------------------------------------
[ GetSub   obj;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (nn == 'inventory')
         return InvSub();
   obj = IdObj(nn);
   if (obj == 0 || obj hasnt carryable) {
      print "It's beyond my power to do that.^";
      rfalse;
   }
   if (obj has carrying) {
      print "I've already got it.^";
      rfalse;
   }
   if (obj notin currentLocation) {
      Icant();
      rfalse;
   }
   if (itemsCarried == 5) {
      print "I'm carrying too much.^";
      rfalse;
   }
   itemsCarried++;
   give obj carrying;
   remove obj;
   print "OK.^";
   rfalse;
];
! ------------------------------------------------------------------------------
[ DropSub   obj;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   obj = IdObj(nn);
   if (obj == 0 || (obj hasnt carrying &&  obj hasnt wearing)) {
      print "I haven't got it.^";
      rfalse;
   }
   itemsCarried--;
   if (currentLocation == canoe_rm) {
      ! in the lake, stuff disappears for good...
      print "It sinks in the Lake.^";
      remove obj;
      give obj ~carrying ~wearing;
      if (nn ~= 'canoe') rfalse;
      print "... so do I!^";
      print "I'm dead!^";
      GameOverSub();
      rfalse;
   }
   if (nn ~= 'mirror') {
      move obj to currentLocation;
      give obj ~carrying ~wearing;
      print "OK.^";
      rfalse;
   }
   ! uh oh - dropped the magic mirror
   if (box notin currentLocation) {
      print "It shatters!^";
      GameOverSub();
      rfalse;
   }
   print "It lands in its Box, lights up, and says:^";
   if (currentLocation ~= storeroom) {
      move mirror to currentLocation;
      give obj ~carrying ~wearing;
      print "Hi there!^";
      rfalse;
   }
   ! dropped it in its box in the storeroom -- that's the ticket...
   print "*Congratulations!* You have won!^^";
   GameOverSub();
   rfalse;
];
! ------------------------------------------------------------------------------
[ InvSub   i n;
   n = 0;
   objectloop (i ofclass Item)
      if (i has carrying || i has wearing) n++;
   if (n == 0)
      print "I am carrying... Nothing at all!^";
   else {
      print "I am carrying ";
      objectloop (i ofclass Item) {
         if (i has carrying || i has wearing) {
            print (string) i.describe;
            if (i has wearing) print " (worn)";
            if (n == 2) print " and ";
            if (n > 2) print ", ";
            n--;
         }
      }
      print ".^";
   }
   rfalse;
];
! ------------------------------------------------------------------------------
[ OpenSub   retval;
   retVal = false;
   if (nn == ',*')
      print "I need a Noun too!^";
   if (nn == 'manhole') {
      if (currentLocation ~= ladder_rm)
         print "Not now ...^";
      else if (~~TestFlag(manholeUnlocked))
            print "It's locked.^";
         else {
            SetFlag(manholeOpen);
            manhole.describe = "an open manhole";
            print "OK.^";
         }
   }
   else if (nn == 'window') {
      if (currentLocation ~= attic)
         print "Not now ...^";
      else {
         window.describe = "an open window";
         setflag(windowOpen);
         SetFlag(promptEnteringRoom);
         retVal = true;
      }
   }
   else
      Icant();
   return retVal;
];
! ------------------------------------------------------------------------------
[ FollowSub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (nn ~= 'path') {
      Icant();
      rfalse;
   }
   if (currentLocation ~= garden2) {
      print "Not now ...^";
      rfalse;
   }
   currentLocation = pathend;
   rtrue;
];
! ------------------------------------------------------------------------------
[ MoveSub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (nn == 'painting' || nn == 'picture') {
      if (currentLocation ~= bedchamber) {
         print "Not now ...^";
         rfalse;
      }
      bedchamber.n_to = vault;
      print "There's a passageway behind it!^";
      SetFlag(promptEnteringRoom);
      rtrue;
   }
   if (nn == 'tapestry') {
      if (currentLocation ~= vault) {
         print "Not now ...^";
         rfalse;
      }
      vault.n_to = hiddenroom;
      print "There's a passageway behind it!^";
      SetFlag(promptEnteringRoom);
      rtrue;
   }
   if (nn == 'bed') {
      if (currentLocation ~= bedroom) {
         print "Not now ...^";
         rfalse;
      }
      move trapdoor1 to currentLocation;
      print "I find Something!^";
      SetFlag(promptEnteringRoom);
      rtrue;
   }
   if (nn == 'rock') {
      if (currentLocation ~= tunnel) {
         print "Not now ...^";
         rfalse;
      }
      tunnel.n_to = mustycave;
      print "There's a passageway behind it!^";
      SetFlag(promptEnteringRoom);
      rtrue;
   }
   Icant();
   rfalse;
];
! ------------------------------------------------------------------------------
[ ReadSub;
   if (nn == ',*')
      print "I need a Noun too!^";
   else if (nn ~= 'book')
      Icant();
   else if (book hasnt carrying)
      print "I haven't got it.^";
   else if (spectacles hasnt wearing)
      print "The print is too small to read!^";
   else
      print "It says:^A useful magic word is 'ZONK'.^";
   rfalse;
];
! ------------------------------------------------------------------------------
[ SaySub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   print "OK.   ~", (pbuf) 2, "~^";
   if (nn ~= 'zonk' || (torch hasnt carrying && torch notin currentLocation)) {
      print "Nothing Happens.^";
      rfalse;
   }
   print "The Torch lights up!^";
   torch.describe = "a lit TORCH";
   SetFlag(torchIsLit);
   SetFlag(promptEnteringRoom);
   rtrue;
];
! ------------------------------------------------------------------------------
[ WearSub   obj;
   if (nn == ',*')
      print "I need a Noun too!^";
   if (nn == 'spectacles' || nn == 'aqualung') {
      obj = IdObj(nn);
      if (obj hasnt carrying)
         print "I haven't got it.^";
      else {
         give obj wearing;
         print "OK.^";
      }
   }
   else
      Icant();
   rfalse;
];
! ------------------------------------------------------------------------------
[ ClimbSub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (nn ~= 'ladder') {
      Icant();
      rfalse;
   }
   if (ladder notin currentLocation) {
      print "It's not here!^";
      rfalse;
   }
   if (currentLocation == bysublake) {
      currentLocation = ladder_rm;
      Setflag(promptEnteringRoom);
      rtrue;
   }
   if (currentLocation == hugegarden) {
      currentLocation = attic;
      SetFlag(promptEnteringRoom);
      rtrue;
   }
   print "Not now ...^";
   rfalse;
];
! ------------------------------------------------------------------------------
[ ThrowSub   obj;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   obj = IdObj(nn);
   if (obj == 0 || (obj hasnt carrying && obj hasnt wearing)) {
      print "I haven't got it.^";
      rfalse;
   }
   itemsCarried--;
   if (currentLocation == pathend) {
      print "It falls in the Pond.^";
      move obj to dampchamber;
      give obj ~carrying ~wearing;
   }
   else if (currentLocation == canoe_rm) {
      print "It sinks in the Lake.^";
      remove obj;
      give obj ~carrying ~wearing;
      if (nn == 'canoe') {
         print "... so do I!^";
         GameOverSub();
         rfalse;
      }
   }
   else {
      print "OK.^";
      move obj to currentLocation;
      give obj ~carrying ~wearing;
      print "It falls at my Feet.^";
   }
   rfalse;
];
! ------------------------------------------------------------------------------
[ LookSub;
   if (nn == ',*') rtrue;
   if (nn == 'around') rtrue;
   if (nn == 'tapestry' || nn == 'painting' || nn == 'larder' || nn == 'pool' || nn == 'shack') {
      print "Something's there.^";
      rfalse;
   }
   if ((nn == 'rosebed' || nn == 'bed') && ladder in rose_bed) {
      print "Something's there.^";
      rfalse;
   }
   if (nn == 'palace' || nn == 'garden' || nn == 'window' || nn == 'trapdoor' || nn == 'pond') {
      print "Something's there.^";
      rfalse;
   }
   if (nn == 'cellar') {
      if (currentLocation ~= cellar) {
         print "It's not here!^";
         rfalse;
      }
      if (Testflag(lookedInCellar)) {
         print "I see nothing Special.^";
         rfalse;
      }
      SetFlag(lookedInCellar);
      move box to cellar;
      print "I find Something!^";
      Setflag(promptEnteringRoom);
      rtrue;
   }
   if (nn == 'chasm' || nn == 'lake') {
      print "It's very Deep!^";
      rfalse;
   }
   if (nn == 'ogre' || nn == 'guard') {
      print "He wants Something!^";
      rfalse;
   }
   if (nn ~= 'hedge') {
      print "I see nothing Special.^";
      rfalse;
   }
   if (currentLocation ~= emptyroad) {
      print "It's not here!^";
      rfalse;
   }
   print "I find a path leading East!^";
   emptyroad.e_to = meadow;
   SetFlag(promptEnteringRoom);
   rtrue;
];
! ------------------------------------------------------------------------------
[ SwimSub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (nn == 'lake') {
      if (currentLocation ~= sublake && currentLocation ~= canoe_rm && currentLocation ~= bysublake) {
         print "Not now ...^";
         rfalse;
      }
      print "It's too wide!  I've drowned!^";
      print "I'm dead!^";
      GameOverSub();
      rfalse;
   }
   if (nn ~= 'stream') {
      Icant();
      rfalse;
   }
   if (currentLocation ~= cavern && currentLocation ~= chasm) {
      print "Not now ...^";
      rfalse;
   }
   if (aqualung hasnt wearing) {
      print "No air! ... I drown!^";
      print "I'm dead!^";
      GameOverSub();
      rfalse;
   }
   if (currentLocation == cavern)
      currentLocation = chasm;
   else
      currentLocation = cavern;
   rtrue;
];
! ------------------------------------------------------------------------------
[ JumpSub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (nn == 'down') {
      if (currentLocation ~= ladder_rm) {
         print "Not now ...^";
         rfalse;
      }
      currentLocation = bysublake;
      rtrue;
   }
   if (nn ~= 'chasm') {
      Icant();
      rfalse;
   }
   if (currentLocation ~= ledge && currentLocation ~= chasm) {
      print "Not now ...^";
      rfalse;
   }
   if (itemsCarried > 4) {
      print "I'm carrying too much!^";
      print "I fall and break my Neck!^";
      GameOverSub();
      rfalse;
   }
   if (currentLocation == ledge)
      currentLocation = chasm;
   else
      currentLocation = ledge;
   rtrue;   
];
! ------------------------------------------------------------------------------
[ WaitSub;
   print "Time passes ...^";
   KeyDelay(15);
   if (currentLocation ~= canoe_rm) {
      print "Nothing Happens.^";
      rfalse;
   }
   print "The canoe reaches the Opposite Shore.^";
   riverCrossingDirection = -riverCrossingDirection;
   if (riverCrossingDirection == 1)
      currentLocation = bysublake;
   else
      currentLocation = sublake;
   Setflag(promptEnteringRoom);
   rtrue;
];
! ------------------------------------------------------------------------------
[ UnlockSub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (nn ~= 'manhole') {
      Icant();
      rfalse;
   }
   if (currentLocation ~= ladder_rm) {
      print "Not now ...^";
      rfalse;
   }
   if (key hasnt carrying && key notin rose_bed) {
      print "I need a Key!^";
      rfalse;
   }
   if (TestFlag(manholeUnlocked)) {
      print "It's already unlocked.^";
      rfalse;
   }
   SetFlag(manholeUnlocked);
   print "OK.^";
   rfalse;
];
! ------------------------------------------------------------------------------
[ GiveSub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (currentLocation == palace) {
      if (nn ~= 'medal' || TestFlag(guardIsAdorned)) {
         print "Not now ...^";
         rfalse;
      }
      if (medal hasnt carrying) {
         print "I haven't got it.^";
         rfalse;
      }
      remove medal;
      remove guard;
      SetFlag(guardIsAdorned);
      itemsCarried--;
      rtrue;
   }
   if (currentLocation ~= ledge) {
      print "Not now ...^";
      rfalse;
   }
   if (nn ~= 'whisky' || TestFlag(ogreQuelled)) {
      print "I'm dead!^";
      GameOverSub();
      rfalse;
   }
   if (whisky hasnt carrying) {
      print "I haven't got it.^";
      rfalse;
   }
   remove whisky;
   remove ogre;
   itemsCarried--;
   SetFlag(ogreQuelled);
   rtrue;
];
! ------------------------------------------------------------------------------
[ killSub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (nn ~= 'guard' && nn ~= 'ogre' && nn ~= 'dwarf') {
      Icant();
      rfalse;
   }
   if (sword hasnt carrying) {
      print "I need a Sword!^";
      rfalse;
   }
   if (nn == 'dwarf') {
      if (currentLocation ~= roadbend) {
         print "It's not here!^";
         rfalse;
      }
      print "OK.^";
      SetFlag(dwarfIsDead);
      dwarf.describe = "a dead dwarf";
      setFlag(promptEnteringRoom);
      rtrue;
   }
   if ((nn == 'guard' && currentLocation ~= palace) || (nn == 'ogre' && currentLocation ~= ledge)) {
      print "It's not here!^";
      rfalse;
   }
   print "He survives my attack,and strikes back!^";
   print "I'm dead!^";
   GameOverSub();
   rfalse;
];
! ------------------------------------------------------------------------------
[ ChopSub;
   if (nn == ',*') {
      print "I need a Noun too!^";
      rfalse;
   }
   if (axe hasnt carrying) {
      print "I need an Axe!^";
      rfalse;
   }
   if (nn ~= 'tree' && nn ~= 'forest') {
      Icant();
      rfalse;
   }
   if (currentLocation ~= hillside) {
      print "It's not here!^";
      rfalse;
   }
   print "I find a Secret Path!^";
   hillside.n_to = quarry;
   forest.describe = "an impenetrable forest with a secret path";
   SetFlag(promptEnteringRoom);
   rtrue;
];
! ------------------------------------------------------------------------------
verb 'help' *                         -> Help;
verb 'quit' 'q//' *                   -> Gameover;
verb 'wait' *                         -> Wait;
verb 'save' *                         -> Savegame;
verb 'load' *                         -> Readsavedgame;
verb 'inventory' 'i//' 'inv' 'list' * -> Inv;
verb 'get' 'take' 'g//' *             -> Get;
verb 'drop' 'leave' 'd//' *           -> Drop;
verb 'open'  *                        -> Open;
verb 'follow' *                       -> Follow;
verb 'move' *                         -> Move;
verb 'read' *                         -> Read;
verb 'say' *                          -> Say;
verb 'wear' *                         -> Wear;
verb 'climb' *                        -> Climb;
verb 'throw' *                        -> Throw;
verb 'look' 'l//' 'x//' 'examine' *   -> Look;
verb 'swim' *                         -> Swim;
verb 'jump' *                         -> Jump;
verb 'go'   *                         -> Go;
verb 'unlock' *                       -> Unlock;
verb 'give' *                         -> Give;
verb 'kill' *                         -> kill;
verb 'chop' 'cut' *                   -> Chop;
verb 'scripton'  *                    -> ScriptOn;
verb 'scriptoff' *                    -> ScriptOff;
verb 'cmdread'  *                     -> CommandsRead;
verb 'verbs' 'verb' *                 -> PfVerbs;
! ------------------------------------------------------------------------------
#Endif; ! TARGET_ZCODE
! ------------------------------------------------------------------------------
