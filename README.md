**Magic Mirror**  
Conversion du jeu Magic Mirror (1982) de Michael Taylor,  en langage Basic,  pour le Commodore VIC-20,  
en Inform 6, à partir de la version Python (2015) de Dave Smythe.  

Conversion de la version Python 2 de Dave Smythe en Python 3.  

**Crédits**  
Michael Taylor, 1982, pour le jeu original : 
http://www.miketaylor.org.uk/tech/advent/mm/  
Dave Smythe, 2015, pour la version Python.  
Auraes, 2017-2024, pour l'adaptation en Z-code et la conversion en Python 3.  


